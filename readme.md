Vagrant Base box
================
This is a basic config for a vagrant box with Centos, Apache and MySQL.
And the option for php 5.3/5.4/5.5/5.6

Configuration / Setup
--

1.  Create a directory source and place your source in this directory. (This is ignored in the .gitignore file so you can create a new git repository in this directory)

2. Open the Vagrantfile
3. Change the ipadres to the local ip adres you want to use for this box.
4.  **Optional** remove the # for config.vm.network :public_network to let the box appear on the network
5.  Save and close the Vargantfile

6.  Open config/httpd.conf
7.  Edit DocumentRoot (bottom of the file) to match the public directory of your website (/vagrant/source is the new created source map )
8.  Save and close config/httpd.conf

9. Open script.sh
10. **optional** Remove the # for the wanted PHP version (default is PHP 5.6 )
11. **optional** add new yum packages to install
12. **optional** Add a script that need to run on setup (see the last line as example)

13. replace start.sql with a sql file that need to be imported in your database

14. In your console type: vagrant up

The box will start and you can use it  

Usage
--
- Apache on port 80 (default)
- MySQL on port 3306 (default) user: root without password
- MailCatcher on port 1080 (default) php will use port 1025 for smtp. So you can see the mail in MailCatcher

