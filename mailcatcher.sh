#!/bin/bash

yum install -y centos-release-SCL
yum install -y ruby193 ruby193-ruby-devel
echo 'export PATH=/opt/rh/ruby193/root/usr/local/bin${PATH:+:${PATH}}' >> /opt/rh/ruby193/enable
source /opt/rh/ruby193/enable
echo "source /opt/rh/ruby193/enable" | sudo tee -a /etc/profile.d/ruby193.sh
gem install --no-rdoc --no-ri sinatra
gem install --no-rdoc --no-ri mime-types -v 1.25.1
gem install --no-rdoc --no-ri mailcatcher
mailcatcher --ip=0.0.0.0
sed -i 's/sendmail_path = \/usr\/sbin\/sendmail -t -i/sendmail_path = \/usr\/bin\/env catchmail/g' /etc/php.ini