CREATE DATABASE IF NOT EXISTS demo_database;

USE demo_database;

DROP TABLE IF EXISTS testtable;
CREATE TABLE testtable (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


--
-- Dumping data for table testtable
--

LOCK TABLES testtable WRITE;
/*!40000 ALTER TABLE testtable DISABLE KEYS */;
INSERT INTO testtable VALUES (1,'test'),(2,'test2');
/*!40000 ALTER TABLE testtable ENABLE KEYS */;
UNLOCK TABLES;