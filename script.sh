#!/bin/sh

sudo yum -y install wget

#check if the epel package is present
rpm -qa | grep epel-release-6-5.noarch.rpm &> /dev/null
#if the exit status of the last command is not 0 download and install the epel package
if [ $? -ne 0 ]
then
    cd /tmp
    wget http://dl.iuscommunity.org/pub/ius/stable/CentOS/6/i386/epel-release-6-5.noarch.rpm
    rpm -Uvh epel-release-6-5.noarch.rpm 
    rm -f epel-release-6-5.noarch.rpm
fi

#check if the ius package is present 
rpm -qa | grep ius-release-1.0-14.ius.centos6.noarch &> /dev/null
#if the exit status of the last command is not 0 download and install the ius package
if [ $? -ne 0 ]
then
    cd /tmp
	wget http://dl.iuscommunity.org/pub/ius/stable/CentOS/6/i386/ius-release-1.0-14.ius.centos6.noarch.rpm
    rpm -Uvh ius-release-1.0-14.ius.centos6.noarch.rpm
    rm -f ius-release-1.0-14.ius.centos6.noarch.rpm
fi  

sudo yum -y erase mysql*

sudo yum -y install gcc g++ make automake autoconf curl-devel openssl-devel zlib-devel httpd-devel apr-devel apr-util-devel sqlite-devel

#Install the latest version of Apache 2 from the epel repository
sudo yum -y install httpd
#Install MySQL 55 and the PHP version you want ( php = 5.3 )
sudo yum -y install mysql55-server
#sudo yum -y install php php-mysql php-xml php-mcrypt  php-pecl-xdebug
#sudo yum -y install php54 php54-mysql php54-pdo php54-xml php54-mcrypt  php54-pecl-xdebug
#sudo yum -y install php55u php55u-mysql php55u-pdo php55u-xml php55u-mcrypt php55u-pecl-imagick php55u-soap php55u-xml php55u-json php55u-mbstring php55u-gd  php55u-pecl-xdebug
sudo yum -y install php56u php56u-mysql php56u-pdo php56u-xml php56u-mcrypt php56u-pecl-imagick php56u-soap php56u-xml php56u-json php56u-mbstring php56u-gd php56u-pecl-xdebug

#htop and mlocate are easy to use tools
sudo yum -y install htop mlocate nano git

#composer
curl -sS https://getcomposer.org/installer | php
chmod +x composer.phar
sudo mv composer.phar /usr/local/bin/composer

#replace the default apache configuration
cp /vagrant/config/httpd.conf /etc/httpd/conf

#the default virtual host in httpd.conf is vagrant-host.example.com so add this to the hosts file
cat /etc/hosts | grep vagrant-host.example.com
if [ $? -ne 0 ]
then
	echo "127.0.0.1 vagrant-host.example.com" >> /etc/hosts
fi

#install mailcatcher
sudo -s /vagrant/mailcatcher.sh

#restart apache
sudo service httpd restart
sudo chkconfig --levels 235 httpd on
sudo service mysqld restart
sudo chkconfig --levels 235 mysqld on

#open the firewall (this is not for production!)
sudo iptables -F
sudo iptables -P INPUT ACCEPT
sudo /sbin/service iptables save
sudo service iptables reload

sudo mysql --execute="GRANT ALL ON *.* to root@'%';"

mysql -u root < /vagrant/start.sql

sudo cp /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime
sudo sed -i 's/;date.timezone =/date.timezone =Europe\/Amsterdam/g' /etc/php.ini
 
sudo chmod 0777 /var/lib/php/session

sudo yum -y update

#need to have mlocate installed
sudo updatedb